import React, { useEffect, useState } from 'react';
import { ActivityIndicator, FlatList, Image, View } from 'react-native';

import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Avatar, Button, Card, List, Text, TextInput } from 'react-native-paper';

const HomeScreen = ({ navigation }) => {
  const [filter, setFilter] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [member, setMember] = useState([]);
  const [param, setParam] = useState('');
  const [search, setSearch] = useState('');
  const [sort, setSort] = useState('');

  useEffect(() => {
    getMember(param);
  }, [param]);

  const getMember = async (param) => {
    try {
      const response = await fetch(`https://powerful-reef-47354.herokuapp.com/members/?${param}`);
      const json = await response.json();
      setMember(json);
      setFilter(json);
    } catch (error) {
        console.error(error);
    } finally {
        setLoading(false);
    }
  }
  
  const searchFilter = (text) => {
    if (text) {
      const newData = member.filter((item) => {
        const itemData = (
                item.firstname.th + ' ' + 
                item.firstname.en + ' ' + 
                item.lastname.th + ' ' +
                item.lastname.en + ' ' +
                item.nickname.th + ' ' +
                item.nickname.en
              ).toUpperCase()
        const textData = text.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setFilter(newData);
      setSearch(text);
    } else {setFilter(member);
      setSearch(text);
    }
  }

  const getPropByString = (obj, propString) => {
    if (!propString) return obj;
    var prop, props = propString.split('.');
  
    for (var i = 0, iLen = props.length - 1; i < iLen; i++) {
      prop = props[i];
      
      var candidate = obj[prop];
      if (candidate !== undefined) {
        obj = candidate;
      } else {
        break;
      }
    }
    return obj[props[i]];
  }

  return (
    <View style={{ flex: 1, padding: 24 }}>
      <TextInput value={search} onChangeText={(text) => searchFilter(text)} />
      <List.Section>
        <List.Accordion
          title="Sorting"
          left={props => <List.Icon {...props} icon="sort" />}
        >
          <List.Item title="ชื่อ(ไทย)" onPress={() => setSort('firstname.th')} />
          <List.Item title="ชื่อ(อังกฤษ)" onPress={() => setSort('firstname.en')} />
          <List.Item title="ชื่อเล่น(ไทย)" onPress={() => setSort('nickname.th')} />
          <List.Item title="ชื่อเล่น(อังกฤษ)" onPress={() => setSort('nickname.en')} />
          <List.Item title="วันเกิด" onPress={() => setSort('birthDay')} />
          <List.Item title="รุ่น" onPress={() => setSort('generation')} />
          <List.Item title="ส่วนสูง" onPress={() => setSort('height')} />
        </List.Accordion>
      </List.Section>
      <List.Section>
        <List.Accordion
          title="Filter"
          left={props => <List.Icon {...props} icon="filter" />}
        >
          <List.Item title="ทั้งหมด" onPress={() => {
            setParam('');
          }} />
          <List.Item title="รุ่นที่ 1" onPress={() => setParam('generation=1')} />
          <List.Item title="รุ่นที่ 2" onPress={() => setParam('generation=2')} />
          <List.Item title="ในกรุงเทพ" onPress={() => setFilter(
            member.filter((item) => {
              return item.province === 'Bangkok';
            })
          )} />
          <List.Item title="ต่างจังหวัด" onPress={() => setFilter(
            member.filter((item) => {
              return item.province !== 'Bangkok';
            })
          )} />
        </List.Accordion>
      </List.Section>

      {isLoading ? <ActivityIndicator /> : (
        <FlatList
          data={filter.sort((a, b) => ( getPropByString(a, sort) > getPropByString(b, sort)) ? 1 : -1 )}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({item}) => (
            <Card>
              <View style={{ flexDirection: 'row', flexWrap: 'wrap', padding: 15 }}>
                <Avatar.Image size={90} source={{ uri: `https://murmuring-mountain-39162.herokuapp.com/images/${item.id}/s/${item.image_name}` }} />
                <Card.Content style={{ flex: 1, flexDirection: 'column' }}>
                  <Text>ชื่อ {item.firstname.th} {item.lastname.th}</Text>
                  <Text>ชื่อเล่น {item.nickname.th}</Text>
                  <Card.Actions style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-end' }}>
                    <Button 
                      onPress={() => {
                        navigation.navigate('Details', {
                          itemId: item.id,
                        });
                      }}
                    >
                      รายละเอียดเพิ่มเติม
                    </Button>
                  </Card.Actions>
                </Card.Content>
              </View>
            </Card>
          )}
        />
      )}
    </View>
  );
}

const DetailsScreen = ({ route }) => {
  const [member, setMember] = useState({});
  const { itemId } = route.params;

  useEffect(() => {
    getMember(itemId);
  }, itemId);

  const getMember = async (id) => {
    try {
      const response = await fetch(`https://powerful-reef-47354.herokuapp.com/members/${id}`);
      const json = await response.json();
      setMember(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  return (member && member.firstname) ? (
    <View style={{ flex: 1, padding: 24 }}>
      <Image
        style={{ width: '90%', height: '40%', alignSelf: 'center', marginBottom: 20 }}
        source={{ uri: `https://murmuring-mountain-39162.herokuapp.com/images/${member.id}/s/${member.image_name}` }}
      />
      <Text>ชื่อ-สกุล: {member.firstname.th} {member.lastname.th}</Text>
      <Text>name lastname: {member.firstname.en} {member.lastname.en}</Text>
      <Text>ชื่อเล่น: {member.nickname.th} ({member.nickname.en})</Text>
      <Text>วันเกิด: {member.birthDay}</Text>
      <Text>ส่วนสูง: {member.height}</Text>
      <Text>บ้านเกิด: {member.province}</Text>
      <Text>สิ่งที่ชอบ: {member.likes}</Text>
      <Text>งานอดิเรก: {member.hobbies}</Text>
      <Text>Instagram: {member.instagram}</Text>
      <Text>Generation: {member.generation}</Text>
    </View>
  ) : null;
}

const Stack = createNativeStackNavigator();

export default App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Details" component={DetailsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
